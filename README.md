## a repo for teaching purposes

# how the computer works [ >> see the class files](https://gitlab.com/bhzsolt/teach/-/tree/21-10-23-how-the-cpu-works)

last time we talked about how the computer works.

our programs/algorithms from the most simplistic viewpoint are composed of two parts:
 - some data
 - some operations on those data

this is illustrated in the following code snippet
```
int i;
int x = 5; /* here 5 is some data */

for (i = 0; i < 10; ++i) {
    x = 2 * x - 1; /* this is a line containing operations on that data */
}

```

we have talked about the cpu, and that the programs that we write will fundamentally be translated to a language which the cpu understands. (see the cpu_test.s file, which is a translation of the cpu_test.c program)


for the exercises, i will explain a bit more about how our data is represented,

what operations can we use on those data,

and a bit more about the structure of a programming language, with emphasis on C.

## [continue to the exercise section](https://gitlab.com/bhzsolt/teach/-/tree/fundamentals-of-programming-01)
